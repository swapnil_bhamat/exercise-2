Use FabricJS and create multiple canvases with shapes that can be moved amongst canvases.
Requirements:
[0] the “Generate Canvas” button should randomly generate N number of canvases, where N is
between 3 to 5 canvases.
[1] the user should be able to select the Canvas ID and a Shape from the select menus, and be able to
generate that shape in the chosen canvas.
[2] the generated shape(s) should be transferrable across canvases.