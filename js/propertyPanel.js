var propertyPanel = (function(){
    
    var resetUI = function(){
        document.getElementById('canvasHolder').innerHTML = "";
        document.getElementById('selectCanvas').innerHTML = '<option value="">Select Canvas Id</option>';
        document.getElementById('generateShapes').disabled = true;
    }

    /**
     * This method generate multiple canvases according to the count passed to him.
     * @param {Number} count of canvases that needs to be get generated. 
     */
    var generateCanvasByCount = function(count){
        resetUI();
        for(var i = 0; i < count; i++){
            var canvasId = "canvas_"+ (i + 1);
            dragableCanvas.createCanvasById(canvasId);
            addChildToSelectCanvasDropdown(canvasId);
        }  
    }

    /**
     * In this method we are adding child options node to select canvas dropdown.
     * @param {String} canvasId is the name of child that needs to be get appended to the dropdown.
     */
    var addChildToSelectCanvasDropdown = function(canvasId){
        var selectCanvas = document.getElementById('selectCanvas');
        var optionNode = document.createElement('option');
        optionNode.value = canvasId;
        optionNode.innerHTML = canvasId;
        selectCanvas.appendChild(optionNode);
    }

    /**
     * Generate random number between min to max value.
     * @param {Number} min value 
     * @param {Number} max value
     */
    var getRandomNumber = function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /**
     * Hanlder of generate canvas button
     */
    document.getElementById('generateCanvas').onclick = function(){
        var canvasCount = getRandomNumber(3, 5);
        generateCanvasByCount(canvasCount);
    }

    /**
     * Handler of generate shape button
     */
    document.getElementById('generateShapes').onclick = function(){
        var selectCanvas = document.getElementById('selectCanvas');
        var selectedCanvasId = selectCanvas.options[selectCanvas.selectedIndex].value;
        var selectShape = document.getElementById('selectShape');
        var selectedShape = selectShape.options[selectShape.selectedIndex].value;
        dragableCanvas.addShapeToCanvas(selectedCanvasId, selectedShape);
    }

    /**
     * Handler of select canvas dropdown
     */
    document.getElementById('selectCanvas').onchange = function(e){
       var selectedCanvasId = this.options[this.selectedIndex].value;
       if(selectedCanvasId != ""){
            document.getElementById('generateShapes').disabled = false;
       }
       else{
            document.getElementById('generateShapes').disabled = true;
       }
    }

})();