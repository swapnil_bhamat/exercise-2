var dragableCanvas = (function(){

    var canvasObj = {};

    var dragObject = null;

    /**
     * This method is responsible for generation of fabric canvas of id passed to him.
     * This is public method
     * @param {String} canvasId  
     */
    var createfabricCanvasById = function (canvasId){
        var canvasContainer = document.getElementById('canvasHolder');
        var canvas = document.createElement('canvas');
        canvas.id =  canvasId;
        canvasContainer.appendChild(canvas);
        canvasObj[canvasId] = new fabric.Canvas(canvas, {width: 400, height:300});
        attachEventListner(canvasObj[canvasId]);
    }

    /**
     * This method add shape of type passed to him to the canvas of id given.
     * This is public method.
     * @param {String} canvasId of canvas that needs to add shape
     * @param {String} shapeType 
     */
    var addShapeToCanvas = function(canvasId, shapeType){
        var shapeObj = _getShapeObjectFromName(shapeType);
        var fabricCanvasObj = canvasObj[canvasId];
        fabricCanvasObj.add(shapeObj);
        fabricCanvasObj.setActiveObject(shapeObj);
        fabricCanvasObj.renderAll();
    }

    /**
     * This method return the instance of shape which type has been passed.
     * @param {String} shapeType 
     */
    var _getShapeObjectFromName = function(shapeType){
        var shapeObject = null;
        switch(shapeType){
            case "rect":
            {
                shapeObject = new fabric.Rect({
                    top: 200, 
                    left: 200, 
                    width: 100, 
                    height: 80, 
                    fill: 'red',
                    stroke: '#000', 
                    strokeWidth: 5,
                });
                break
            }
            case "circle":
            {
                shapeObject = new fabric.Circle({
                    radius: 50, 
                    left: 75, 
                    top: 75, 
                    fill: 'red',
                    stroke: '#000', 
                    strokeWidth: 5,
                  });
                break;
            }
            case "triangle":
            {
                shapeObject = new fabric.Triangle({
                    width: 100, 
                    height: 100, 
                    left: 50, 
                    fill: 'red',
                    stroke: '#000', 
                    strokeWidth: 5,
                  });
                break;
            }
            default:{
                shapeObject = new fabric.Rect({
                    top: 200, 
                    left: 200, 
                    width: 100, 
                    height: 80, 
                    fill: 'red',
                    stroke: '#000', 
                    strokeWidth: 5,
                });
            }

        }
        return shapeObject;
    }

     /**
      * This method attaches the event listner to the canvas instance.
      * @param {Object} canvasIns Instance of canvas.
      */
    var attachEventListner = function(canvasIns) {
        canvasIns.observe("object:moving", checkObjectPosition);
        canvasIns.observe("mouse:up", mouseUpHandler);
    }

    /**
     * Handler of object move in canvas.
     * Here we are checking whether the object is outside of canvas
     * if yes then cloning the same in temparary object so that we can add the same in another canvas.
     * And removing it from currrent canvas.
     * @param {Object} event  
     */
     var checkObjectPosition = function(event){
        if(event.target && event.target.canvas && isOutsideOfCanvas(event)){
            var canvas = event.target.canvas;
            var activeObject = canvas.getActiveObject();
            activeObject.clone(function (clone) { dragObject = clone; })
            canvas.remove(activeObject);
        }
     }

     /**
      * Handler of mouse up on canvas.
      * Here we are checking is user is hovering inside the canvas element.
      * And can we have dragged object of another canvas.
      * If yes then we are adding the same in current canvas. 
      * @param {Object} event object 
      */
     var mouseUpHandler = function(event){
       var canvasId = event.e.target.previousSibling.id;
        if (dragObject != null && canvasId) {
                var canvas = canvasObj[canvasId];
                var mouseX = event.e.clientX;
                var mouseY = event.e.clientY;
                if (isInsideTheElement(mouseX, mouseY, canvas.getElement())) {
                    addDragObjectToCanvas(mouseX, mouseY, canvas, dragObject);
                } 
            dragObject = null;
        }
        
     }

     /**
      * This method is adding draged object on canvas according to the mouse position of end user.
      * @param {Number} mouseX of client
      * @param {Number} mouseY of client 
      * @param {Object} canvas instance
      * @param {Object} object that needs to add on canvas 
      */
     var addDragObjectToCanvas = function(mouseX, mouseY, canvas, object){
        var canvasLeft = canvas.getElement().getBoundingClientRect().left;
        var canvasTop = canvas.getElement().getBoundingClientRect().top;
        object.left = (mouseX - canvasLeft);
        object.top = (mouseY - canvasTop);
        object.originX = "center";
        object.originY = "center";
        canvas.add(object);
        canvas.setActiveObject(object);
     }

     /**
      * This helper method tells whether the user is hovering outside of current canvas area.
      * @param {Object} event object 
      */
     var isOutsideOfCanvas = function(event){
        var mouseY = event.e.clientY;
        var mouseX = event.e.clientX; 
        var canvasElement = event.target.canvas.getElement();
        return !isInsideTheElement(mouseX, mouseY, canvasElement);
     }

     /**
      * This helper method check whether the user is hovering inside of element that has been passed.
      * @param {Number} mouseX of client.
      * @param {*} mouseY of client.
      * @param {*} element which has to be checked.
      */
     var isInsideTheElement = function(mouseX, mouseY, element) {
         var elementBoundingRect = element.getBoundingClientRect();
         return (      mouseX > elementBoundingRect.left
                   && mouseX < elementBoundingRect.left + elementBoundingRect.width
                   && mouseY < elementBoundingRect.top + elementBoundingRect.height
                   && mouseY > elementBoundingRect.top
                );    
    }

    return {
        createCanvasById: createfabricCanvasById,
        addShapeToCanvas:addShapeToCanvas
    }

})();